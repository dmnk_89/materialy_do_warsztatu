package pl.warsjawa.wicket.repositories;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.stereotype.Repository;
import pl.warsjawa.wicket.domain.Book;

import java.util.*;

@Repository
public class BookRepository {

    private static Map<Long,Book> booksById = new HashMap<Long, Book>();
    private static Multimap<String,Book> booksByTitle = ArrayListMultimap.create();
    private static Multimap<String,Book> booksByAuthor = ArrayListMultimap.create();

    static {
        long nextId = 1;
        BookRepository r = new BookRepository();
        r.addBook(new Book().author("Jan Kowalski").title("Tytuł1").shortInfo("opis").id(nextId++));
        r.addBook(new Book().author("Jan Kowalski").title("Tytuł2").shortInfo("opis").id(nextId++));
        r.addBook(new Book().author("Jan Kowalski").title("Tytuł3").shortInfo("opis").id(nextId++));
        r.addBook(new Book().author("Jan Kowalski").title("Tytuł4").shortInfo("opis").id(nextId++));
        r.addBook(new Book().author("Jan Kowalski").title("Tytuł5").shortInfo("opis").id(nextId++));
        r.addBook(new Book().author("Jan Kowalski").title("Tytuł6").shortInfo("opis").id(nextId++));
        r.addBook(new Book().author("Jan Kowalski").title("Tytuł7").shortInfo("opis").id(nextId++));
    }

    static Book getBook(Long id){
        return booksById.get(id);
    }

    public Collection<Book> findBooks() {
        return booksById.values();
    }

    public void addBook(Book book) {
        booksById.put(book.getId(), book);
        booksByTitle.put(book.getTitle(), book);
        booksByAuthor.put(book.getAuthor(), book);
    }

    public Book find(long id) {
        return booksById.get(id);
    }
}
