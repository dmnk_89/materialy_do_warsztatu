package pl.warsjawa.wicket.repositories;

import com.google.common.collect.Maps;
import org.springframework.stereotype.Repository;
import pl.warsjawa.wicket.domain.User;

import java.util.Map;

@Repository
public class UserRepository {

    private static Map<String,User> usersByLogin = Maps.newHashMap();
    private static Map<LoginPasswordKey, User> usersByLoginAndPassword = Maps.newHashMap();

    static {
        UserRepository r= new UserRepository();
        r.addUser(new User().login("wicket").password("wicket"));
        r.addUser(new User().login("wicket2").password("wicket2"));
        r.addUser(new User().login("wicket3").password("wicket3"));
    }

    static User getUser(String login){
        return usersByLogin.get(login);
    }

    public void addUser(User user) {
        usersByLogin.put(user.getLogin(), user);
        usersByLoginAndPassword.put(new LoginPasswordKey(user),user);

    }

    public User find(String login, String password) {
        return usersByLoginAndPassword.get(new LoginPasswordKey(login, password));
    }

    private class LoginPasswordKey{
        private String login;
        private String password;

        private LoginPasswordKey(User user) {
            login(user.getLogin());
            password(user.getPassword());
        }

        private LoginPasswordKey(String login, String password) {
            this.login = login;
            this.password = password;
        }

        public LoginPasswordKey login(final String login) {
            this.login = login;
            return this;
        }

        public LoginPasswordKey password(final String password) {
            this.password = password;
            return this;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            LoginPasswordKey that = (LoginPasswordKey) o;

            if (login != null ? !login.equals(that.login) : that.login != null) return false;
            if (password != null ? !password.equals(that.password) : that.password != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = login != null ? login.hashCode() : 0;
            result = 31 * result + (password != null ? password.hashCode() : 0);
            return result;
        }
    }

}
