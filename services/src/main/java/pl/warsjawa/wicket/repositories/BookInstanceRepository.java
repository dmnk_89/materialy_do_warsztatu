package pl.warsjawa.wicket.repositories;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import org.springframework.stereotype.Repository;
import pl.warsjawa.wicket.domain.Book;
import pl.warsjawa.wicket.domain.BookInstance;
import pl.warsjawa.wicket.domain.User;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static pl.warsjawa.wicket.repositories.BookRepository.getBook;
import static pl.warsjawa.wicket.repositories.UserRepository.getUser;

@Repository
public class BookInstanceRepository {

    private static Map<Long, BookInstance> bookInstanceById = Maps.newHashMap();
    private static Multimap<Book, BookInstance> bookInstanceByBook = ArrayListMultimap.create();
    private static Multimap<User, BookInstance> bookInstanceByOwner = ArrayListMultimap.create();
    private static Multimap<User, BookInstance> bookInstanceByPossessor = ArrayListMultimap.create();


    static {
        long id = 1;
       BookInstanceRepository r = new BookInstanceRepository();
        r.addBookInstance(new BookInstance()
                .id(id++).book(getBook(1l))
                .owner(getUser("wicket")));
        r.addBookInstance(new BookInstance()
                .id(id++).book(getBook(1l))
                .owner(getUser("wicket2"))
                .currentPossessor(getUser("wicket")));
        r.addBookInstance(new BookInstance()
                .id(id++).book(getBook(2l))
                .owner(getUser("wicket")));
        r.addBookInstance(new BookInstance()
                .id(id++).book(getBook(2l))
                .owner(getUser("wicket2"))
                .currentPossessor(getUser("wicket")));
        r.addBookInstance(new BookInstance()
                .id(id++).book(getBook(3l))
                .owner(getUser("wicket3"))
                .currentPossessor(getUser("wicket")));
    }


    public void addBookInstance(BookInstance bookInstance){
        bookInstanceById.put(bookInstance.getId(),bookInstance);
        bookInstanceByBook.put(bookInstance.getBook(),bookInstance);
        bookInstanceByOwner.put(bookInstance.getOwner(),bookInstance);
        bookInstanceByPossessor.put(bookInstance.getCurrentPossessor(),bookInstance);
    }


    public Collection<BookInstance> findByPossessor(User user) {
        return bookInstanceByPossessor.get(user);
    }

    public Collection<BookInstance> findByOwner(User user) {
        return bookInstanceByOwner.get(user);
    }
}

