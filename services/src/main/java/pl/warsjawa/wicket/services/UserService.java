package pl.warsjawa.wicket.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.warsjawa.wicket.domain.User;
import pl.warsjawa.wicket.repositories.UserRepository;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public User login(String login, String password){
        return repository.find(login, password);
    }

}
