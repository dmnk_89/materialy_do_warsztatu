package pl.warsjawa.wicket.services;

import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.warsjawa.wicket.domain.BookInstance;
import pl.warsjawa.wicket.domain.User;
import pl.warsjawa.wicket.repositories.BookInstanceRepository;

import java.util.Collection;
import java.util.List;

@Service
public class BookInstanceService {

    @Autowired
    private BookInstanceRepository repository;


    public List<BookInstance> findPossessedByUser(User user) {
        return ImmutableList.copyOf(repository.findByPossessor(user));
    }

    public List<BookInstance> findOwnedUser(User user) {
        return ImmutableList.copyOf(repository.findByOwner(user));    }
}
