package pl.warsjawa.wicket.services;

import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.warsjawa.wicket.domain.Book;
import pl.warsjawa.wicket.repositories.BookRepository;
import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepository repository;

    public List<Book> findBooks(){
        return ImmutableList.copyOf(repository.findBooks());
    }

}
