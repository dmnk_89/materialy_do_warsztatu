package pl.warsjawa.wicket.domain;

/**
 * Created with IntelliJ IDEA.
 * User: dwr
 * Date: 07.10.13
 * Time: 23:45
 * To change this template use File | Settings | File Templates.
 */
public class User {
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User login(final String login) {
        this.login = login;
        return this;
    }

    public User password(final String password) {
        this.password = password;
        return this;
    }


}
