package pl.warsjawa.wicket.domain;

import java.io.Serializable;

public class Book implements Serializable {

    private long id;

    private String title;
    private String author;
    private String shortInfo;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getShortInfo() {
        return shortInfo;
    }

    public void setShortInfo(String shortInfo) {
        this.shortInfo = shortInfo;
    }

    public Book id(final long id) {
        this.id = id;
        return this;
    }

    public Book shortInfo(final String shortInfo) {
        this.shortInfo = shortInfo;
        return this;
    }


    public Book title(final String title) {
        this.title = title;
        return this;
    }

    public Book author(final String author) {
        this.author = author;
        return this;
    }


}
