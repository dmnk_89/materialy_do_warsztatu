package pl.warsjawa.wicket.domain;

import java.io.Serializable;

public class BookInstance implements Serializable {

    private long id;

    private User owner;
    private User currentPossessor;
    private Book book;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public User getCurrentPossessor() {
        return currentPossessor;
    }

    public void setCurrentPossessor(User currentPossessor) {
        this.currentPossessor = currentPossessor;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public BookInstance id(final long id) {
        this.id = id;
        return this;
    }

    public BookInstance owner(final User owner) {
        this.owner = owner;
        return this;
    }

    public BookInstance currentPossessor(final User currentPossessor) {
        this.currentPossessor = currentPossessor;
        return this;
    }

    public BookInstance book(final Book book) {
        this.book = book;
        return this;
    }


}
