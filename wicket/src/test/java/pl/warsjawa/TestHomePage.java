package pl.warsjawa;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.warsjawa.wicket.web.pages.HomePage;
import pl.warsjawa.wicket.web.JugotekaApplication;

/**
 * Simple test using the WicketTester
 */

@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class TestHomePage
{
	private WicketTester tester;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private JugotekaApplication myWebApplication;

    @Before
    public void setUp() {
        tester = new WicketTester(myWebApplication);
    }

	@Test
	public void homepageRendersSuccessfully()
	{
		//start and render the test page
		tester.startPage(HomePage.class);

		//assert rendered page class
		tester.assertRenderedPage(HomePage.class);
	}
}
