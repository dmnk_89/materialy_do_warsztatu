package pl.warsjawa.wicket.web.pages;


import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.warsjawa.wicket.services.BookService;

public class HomePage extends WebPage {

    private static final Logger log = LoggerFactory.getLogger(HomePage.class);


    @SpringBean
    BookService service;

	public HomePage() {
        log.error(service.findBooks().toString());

    }


}
