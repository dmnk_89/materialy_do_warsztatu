package pl.warsjawa.wicket.web;

import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import pl.warsjawa.wicket.web.pages.HomePage;


@Component
public class JugotekaApplication extends WebApplication implements ApplicationContextAware {
    private static final Logger log = LoggerFactory.getLogger(JugotekaApplication.class);
    private ApplicationContext ctx;

    /**
     * @see org.apache.wicket.Application#getHomePage()
     */
    @Override
    public Class<? extends WebPage> getHomePage() {
        return HomePage.class;
    }

    /**
     * @see org.apache.wicket.Application#init()
     */
    @Override
    public void init() {

        super.init();
        getComponentInstantiationListeners().add(new SpringComponentInjector(this, ctx));



        RuntimeConfigurationType configurationType = getConfigurationType();
        if (RuntimeConfigurationType.DEVELOPMENT.equals(configurationType)) {
            log.info("You are in DEVELOPMENT mode!");
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }


}